//
//  CITConfig.h
//  MarvelTesteCIT
//
//  Created by André Campopiano on 26/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CITConfig : NSObject

+ (instancetype) instance;
+ (id) alloc __attribute__((unavailable("alloc not available")));

-(NSString*) host;
-(NSString*) apiBasePath;
-(NSString*) protocol;
-(NSString*) privateKey;
-(NSString*) publicKey;
-(NSString*) limit;
@end
