//
//  CITConfig.m
//  MarvelTesteCIT
//
//  Created by André Campopiano on 26/05/17.
//  Copyright © 2016 André Campopiano. All rights reserved.
//

#import "CITConfig.h"

@interface CITConfig(){
    NSDictionary *config;
}

@end

@implementation CITConfig

static CITConfig * instance = nil;

+(instancetype)instance{
    @synchronized ([CITConfig class]) {
        if(instance == nil){
            instance = [[super alloc] init];
        }
        return instance;
    };
    return nil;
}

-(id) init {
    if (self = [super init]){
        NSString *configPath = [[NSBundle mainBundle] pathForResource:@"Config" ofType:@"plist"];
        config = [[NSDictionary alloc] initWithContentsOfFile:configPath];
    }
    return self;
}

-(NSString *)host{
    return [config objectForKey:@"host"];
}

-(NSString *)apiBasePath{
    return [config objectForKey:@"api_base_path"];
}

-(NSString *)protocol{
    return [config objectForKey:@"protocol"];
}

-(NSString *)privateKey{
    return [config objectForKey:@"privateKey"];
}

-(NSString *)publicKey{
    return [config objectForKey:@"publicKey"];
}
-(NSString *)limit{
    return [config objectForKey:@"limit"];
}

@end
