//
//  CITManager.h
//  MarvelTesteCIT
//
//  Created by André Campopiano on 26/05/17.
//  Copyright © 2016 André Campopiano. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol INetworkManager <NSObject>
- (void) onChangeStatusNetwork:(BOOL)available;
@end

@interface CITManager : NSObject

+ (instancetype) instance;
+ (id) alloc __attribute__((unavailable("alloc not available")));
- (void) addObserverNetworkManager:(NSObject<INetworkManager>*)delegate;
- (void) removeObserverNetworkManager:(NSObject<INetworkManager>*)delegate;

@end
