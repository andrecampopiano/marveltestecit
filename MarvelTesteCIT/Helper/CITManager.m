//
//  CITManager.m
//  MarvelTesteCIT
//
//  Created by André Campopiano on 26/05/17.
//  Copyright © 2016 André Campopiano. All rights reserved.
//

#import "CITManager.h"
#import "Reachability.h"

@interface CITManager(){
    BOOL _available;
    BOOL _notify;
}

@property (nonatomic) Reachability *hostReachability;
@property (nonatomic) Reachability *internetReachability;
@property (nonatomic) Reachability *wifiReachability;
@property (nonatomic) NSMutableArray<INetworkManager> *observerList;


@end

@implementation CITManager

static CITManager *instance = nil;

+ (instancetype) instance {
    @synchronized([CITManager class]) {
        if (instance == nil) {
            instance = [[super alloc] init];
        }
        return instance;
    };
    return nil;
}

- (id) init {
    if (self = [super init]) {
        _available = YES;
    }

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    
    
    NSString *remoteHostName = @"www.apple.com";
    
    self.hostReachability = [Reachability reachabilityWithHostName:remoteHostName];
    [self.hostReachability startNotifier];
    [self updateInterfaceWithReachability:self.hostReachability];
    
    self.internetReachability = [Reachability reachabilityForInternetConnection];
    [self.internetReachability startNotifier];
    [self updateInterfaceWithReachability:self.internetReachability];
    
    self.wifiReachability = [Reachability reachabilityForLocalWiFi];
    [self.wifiReachability startNotifier];
    [self updateInterfaceWithReachability:self.wifiReachability];
    
    
    return self;
}

- (void) reachabilityChanged:(NSNotification *)note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    [self updateInterfaceWithReachability:curReach];
}


- (void)updateInterfaceWithReachability:(Reachability *)reachability
{
    _notify = NO;
    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    switch (netStatus)
    {
        case NotReachable:
            _available = NO;
            [self notifyObserver];
            break;
        case ReachableViaWWAN:
        case ReachableViaWiFi:
            _available = YES;
            [self notifyObserver];
        break;
    }
}

- (void)notifyObserver{
    
    if (_observerList && (_observerList.count > 0)) {
        if (!_notify) {
            for (int i = 0; i < _observerList.count; i++) {
                [_observerList[i] onChangeStatusNetwork:_available];
            }
        }
        _notify = YES;
    }
}


- (void) addObserverNetworkManager:(NSObject<INetworkManager>*)delegate{
    if (delegate) {
        if (!_observerList) {
            _observerList = [@[]mutableCopy];
        }
        [_observerList addObject:delegate];
        [delegate onChangeStatusNetwork:_available];
        [self notifyObserver];
    }
}

- (void) removeObserverNetworkManager:(NSObject<INetworkManager>*)delegate{
    
    if (_observerList && (_observerList.count > 0)) {
        [_observerList removeObject:delegate];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

@end
