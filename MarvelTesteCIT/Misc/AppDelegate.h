//
//  AppDelegate.h
//  MarvelTesteCIT
//
//  Created by André Campopiano on 26/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property(strong, nonatomic, readonly) NSManagedObjectContext *managedObjectContext;
@property(strong, nonatomic, readonly) NSManagedObjectModel *managedObjectModel;
@property(strong, nonatomic, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;
-(NSURL *)applicationDocumentsDirectory;

@end

