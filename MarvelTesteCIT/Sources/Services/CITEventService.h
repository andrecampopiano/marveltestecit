//
//  CITEventService.h
//  MarvelTesteCIT
//
//  Created by André Campopiano on 27/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CITEvents.h"
#import "CITManager.h"

@interface CITEventService : NSObject<INetworkManager>

+(instancetype) instance;
-(void)recoveryEventWithHero:(NSNumber*)idHero callback:(void (^)(NSMutableArray<CITEvents> *listEvents, NSString *erro))callback;


@end
