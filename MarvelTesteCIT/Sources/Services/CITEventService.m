//
//  CITEventService.m
//  MarvelTesteCIT
//
//  Created by André Campopiano on 27/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import "CITEventService.h"
#import "CITConector.h"

@interface CITEventService(){
    BOOL _available;
}
@end

@implementation CITEventService

static CITEventService *instance = nil;

+(instancetype) instance {
    @synchronized ([CITEventService class]) {
        if (instance == nil){
            instance = [[super alloc]init];
        }
        return instance;
    };
    return nil;
}

-(id)init{
    if (self = [super init]){
        [[CITManager instance] addObserverNetworkManager:self];
    }
    return self;
}

-(void)onChangeStatusNetwork:(BOOL)available{
    _available = available;
}


-(void)recoveryEventWithHero:(NSNumber *)idHero callback:(void (^)(NSMutableArray<CITEvents> *, NSString *))callback{
    if(_available){
        NSString *call = [NSString stringWithFormat:@"characters/%@/events?",idHero];
        
        [[CITConector instance] call:call method:CITConnectionTypeGet body:nil completion:^(NSURLResponse *response, id data, NSError *connectionError) {
            NSString *strError = nil;
            NSMutableArray <CITEvents> *array;
            if(connectionError){
                strError = [connectionError.userInfo objectForKey:@"NSLocalizedDescription"];
            }else{
                NSError *error;
                NSMutableDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                array = (NSMutableArray<CITEvents>*)[CITEvents arrayOfModelsFromDictionaries:[NSArray arrayWithObject:dic[@"data"][@"results"]] error:&error];
                if (error) {
                    strError = error.description;
                }
            }
        callback(array, strError);
        }];
    }else {
        #warning colocar no Localization String
        callback(nil,@"se, conexão");
    }
}


@end
