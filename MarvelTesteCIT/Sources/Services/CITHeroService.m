//
//  CITHeroService.m
//  MarvelTesteCIT
//
//  Created by André Campopiano on 26/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import "CITHeroService.h"
#import "CITConector.h"
#import "CITHeroDBCoreData.h"

@interface CITHeroService(){
    
    BOOL _available;
}


@end

@implementation CITHeroService


static CITHeroService *instance = nil;
+ (instancetype) instance {
    @synchronized([CITHeroService class]) {
        if (instance == nil) {
            instance = [[super alloc] init];
        }
        return instance;
    };
    return nil;
}

-(id)init{
    if (self = [super init]){
        [[CITManager instance] addObserverNetworkManager:self];
    }
    return self;
}

-(void)onChangeStatusNetwork:(BOOL)available{
    _available = available;
}


#pragma mark - Busca herois do banco e junta com os da api
-(void)recoveryHeroesDbAndApiWithOffSet:(NSInteger)offSet callback:(void (^)(NSMutableArray<CITHero> *listHero, NSString *erro))callback{
    CITHeroDBCoreData *db = [CITHeroDBCoreData new];
    NSMutableArray<CITHero> *listAllHero = [NSMutableArray<CITHero> new];
    NSArray<CITHeroEntity *> *listHeroEntity = [db getAllHeros];
    for(CITHeroEntity *heroEntity in listHeroEntity){
        CITHero *hero = [self setNewHero:heroEntity];
        [listAllHero addObject:hero];
    }
    [self recoveryHerosWithOffSet:offSet callback:^(NSMutableArray<CITHero> *listHero, NSString *erro) {
        for (CITHero *hero in listHero){
            [listAllHero addObject:hero];
        }
        callback(listAllHero,erro);
    }];
}

#pragma mark - Busca todos herois da api
- (void)recoveryHerosWithOffSet:(NSInteger)offset callback:(void (^)(NSMutableArray<CITHero> *listHero,  NSString *erro))callback{
    if(_available){
        NSString *call = [NSString stringWithFormat:@"characters?offset=%ld&",offset];
        [[CITConector instance] call:call method:CITConnectionTypeGet body:nil completion:^(NSURLResponse *response, id data, NSError *connectionError) {
            NSString *strError = nil;
            NSMutableArray <CITHero> *listHero;
            if (connectionError) {
                strError = [connectionError.userInfo objectForKey:@"NSLocalizedDescription"];
            } else {
                NSError *error;
                NSMutableDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                listHero = (NSMutableArray<CITHero>*)[CITHero arrayOfModelsFromDictionaries:[NSArray arrayWithObject:dic[@"data"][@"results"]] error:&error];
                CITHeroDBCoreData *db = [CITHeroDBCoreData new];
                NSMutableArray<CITHero> *newList = [NSMutableArray<CITHero> new];
                [newList addObjectsFromArray:listHero];
                for(CITHero *hero in listHero){
                    if([db getHeroById:[hero.id stringValue]]){
                        [newList removeObject:hero];
                    }
                }
                if (error) {
                    strError = error.description;
                }
                listHero = newList;
            }
            callback(listHero, strError);
        }];
    }else{
        NSMutableArray <CITHero> *listHero = [self recoveryHerosDB];
        callback(listHero,@"No internet connection");
    }
    
}

#pragma mark - Busca Heroes por filtro de nome
-(void)recoveryHerosWithName:(NSString *)name callback:(void (^)(NSMutableArray<CITHero> *listHero, NSString *erro))callback{
    if(_available){
        NSString *call = [NSString stringWithFormat:@"characters?nameStartsWith=%@&limit=100&",name];
        [[CITConector instance] call:call method:CITConnectionTypeGet body:nil completion:^(NSURLResponse *response, id data, NSError *connectionError) {
            NSString *strError = nil;
            NSMutableArray <CITHero> *listHero;
            if (connectionError) {
                strError = [connectionError.userInfo objectForKey:@"NSLocalizedDescription"];
            } else {
                NSError *error;
                NSMutableDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                listHero = (NSMutableArray<CITHero>*)[CITHero arrayOfModelsFromDictionaries:[NSArray arrayWithObject:dic[@"data"][@"results"]] error:&error];
                CITHeroDBCoreData *db = [CITHeroDBCoreData new];
                NSMutableArray<CITHero> *newList = [NSMutableArray<CITHero> new];
                [newList addObjectsFromArray:listHero];
                for(CITHero *hero in listHero){
                    if([db getHeroById:[hero.id stringValue]]){
                        [newList removeObject:hero];
                    }
                }
                if (error) {
                    strError = error.description;
                }
                listHero = newList;
            }
            callback(listHero, strError);
        }];
    }else{
        callback(nil,@"No internet connection");
    }
}



#pragma mark - Busca todos os itens do banco
-(NSMutableArray<CITHero>*)recoveryHerosDB{
    CITHeroDBCoreData *db = [CITHeroDBCoreData new];
    NSArray<CITHeroEntity*> *listHeroEntity = [db getAllHeros];
    NSMutableArray<CITHero> *listHero = [NSMutableArray<CITHero> new];
    for (CITHeroEntity *heroEntity in listHeroEntity){
        CITHero *hero = [self setNewHero:heroEntity];
        [listHero addObject:hero];
    }
    return listHero;
}

-(void)saveHero:(CITHero *)hero callback:(void (^)(NSString *error))callback{
    CITHeroDBCoreData *db = [CITHeroDBCoreData new];
    CITHeroEntity *newHero = [self setNewHeroEntity:hero];
    [db save:newHero callback:^(NSString *error) {
        if(error){
            callback(error);
        }else{
            callback(nil);
        }
    }];
}

-(void)deleteHero:(NSString *)idHero callback:(void (^)(NSString *))callback{
    CITHeroDBCoreData *db = [CITHeroDBCoreData new];
    CITHeroEntity *heroEntity = [db getHeroById:idHero];
    [db deleteHero:heroEntity callback:^(NSString *error) {
        callback(error);
    }];
}

-(CITHeroEntity*)setNewHeroEntity:(CITHero*)hero{
    CITHeroEntity *newHero = (CITHeroEntity*)[CITHeroDBCoreData newInstanceForClass:[CITHeroEntity class]];
    [newHero setName:hero.name];
    [newHero setId:[hero.id stringValue]];
    [newHero setModified:hero.modified];
    [newHero setResourceURI:hero.resourceURI];
    [newHero setFavorite:YES];
    [newHero setDescr:hero.descr];
    CITThumbnailEntity *thumbnail = (CITThumbnailEntity*)[CITHeroDBCoreData newInstanceForClass:[CITThumbnailEntity class]];
    [thumbnail setPath:hero.thumbnail.path];
    [thumbnail setExtension:hero.thumbnail.extension];
    newHero.thumbnail = thumbnail;
    return newHero;
}

-(CITHero*)setNewHero:(CITHeroEntity*)heroEntity{
    CITHero *hero = [CITHero new];
    [hero setName:heroEntity.name];
    [hero setId:[NSNumber numberWithInteger:[heroEntity.id integerValue]]];
    [hero setFavorite:heroEntity.favorite];
    [hero setModified:heroEntity.modified];
    [hero setDescr:heroEntity.descr];
    [hero setResourceURI:heroEntity.resourceURI];
    CITThumbnail *thumbnail = [CITThumbnail new];
    [thumbnail setPath:heroEntity.thumbnail.path];
    [thumbnail setExtension:heroEntity.thumbnail.extension];
    [hero setThumbnail:thumbnail];
    return hero;
}


@end
