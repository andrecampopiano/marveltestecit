//
//  CITHeroService.h
//  MarvelTesteCIT
//
//  Created by André Campopiano on 26/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CITHero.h"
#import "CITManager.h"


@interface CITHeroService : NSObject<INetworkManager>

+ (instancetype) instance;
- (void)recoveryHerosWithOffSet:(NSInteger)offset callback:(void (^)(NSMutableArray<CITHero> *listHero, NSString *erro))callback;
-(void)saveHero:(CITHero*)hero callback:(void (^)(NSString *erro))callback;
-(void)deleteHero:(NSString *)idHero callback:(void (^)(NSString *))callback;
-(NSMutableArray<CITHero>*)recoveryHerosDB;
-(void)recoveryHeroesDbAndApiWithOffSet:(NSInteger)offset callback:(void (^)(NSMutableArray<CITHero> *listHero, NSString *erro))callback;

-(void)recoveryHerosWithName:(NSString *)name callback:(void (^)(NSMutableArray<CITHero> *listHero, NSString *erro))callback;

@end
