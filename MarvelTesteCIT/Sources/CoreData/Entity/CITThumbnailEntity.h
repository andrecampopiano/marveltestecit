//
//  CITThumbnailEntity.h
//  MarvelTesteCIT
//
//  Created by André Campopiano on 27/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface CITThumbnailEntity : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "CITThumbnailEntity+CoreDataProperties.h"
