//
//  CITHeroEntity+CoreDataProperties.m
//  MarvelTesteCIT
//
//  Created by André Campopiano on 27/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import "CITHeroEntity+CoreDataProperties.h"

@implementation CITHeroEntity (CoreDataProperties)
@dynamic id;
@dynamic name;
@dynamic modified;
@dynamic resourceURI;
@dynamic favorite;
@dynamic descr;
@dynamic thumbnail;
@end
