//
//  CITHeroEntity+CoreDataProperties.h
//  MarvelTesteCIT
//
//  Created by André Campopiano on 27/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import "CITHeroEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface CITHeroEntity (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *id;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *modified;
@property (nullable, nonatomic, retain) NSString *resourceURI;
@property (nullable, nonatomic, retain) NSString *descr;
@property (nonatomic, assign) BOOL favorite;
@property (nullable, nonatomic, retain) CITThumbnailEntity *thumbnail;

@end

@interface CITHeroEntity (CoreDataGeneratedAccessors)

-(void)addThumbnail:(NSSet *)objects;
-(void)addThumbnailObject:(CITThumbnailEntity *)object;
-(void)removeThumbnail:(NSSet *)objects;
-(void)removeThumbnailObject:(CITThumbnailEntity *)object;




@end

NS_ASSUME_NONNULL_END
