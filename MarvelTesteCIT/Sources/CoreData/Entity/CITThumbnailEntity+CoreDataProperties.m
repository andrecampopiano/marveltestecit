//
//  CITThumbnailEntity+CoreDataProperties.m
//  MarvelTesteCIT
//
//  Created by André Campopiano on 28/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import "CITThumbnailEntity+CoreDataProperties.h"

@implementation CITThumbnailEntity (CoreDataProperties)

@dynamic extension;
@dynamic path;

@end
