//
//  CITThumbnailEntity+CoreDataProperties.h
//  MarvelTesteCIT
//
//  Created by André Campopiano on 28/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import "CITThumbnailEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface CITThumbnailEntity (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *path;
@property (nullable, nonatomic, retain) NSString *extension;

NS_ASSUME_NONNULL_END


@end
