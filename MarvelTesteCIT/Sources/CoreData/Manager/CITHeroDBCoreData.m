//
//  CITHeroDBCoreData.m
//  MarvelTesteCIT
//
//  Created by André Campopiano on 28/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import "AppDelegate.h"
#import "CITHeroDBCoreData.h"
@interface CITHeroDBCoreData(){
    
}
+(NSManagedObjectContext*)getContext;
@end

@implementation CITHeroDBCoreData

+(id)newInstanceForClass:(Class)classe{
    NSManagedObjectContext *context = [CITHeroDBCoreData getContext];
    NSString *stringClass = NSStringFromClass([classe class]);
    id c = [NSEntityDescription insertNewObjectForEntityForName:stringClass inManagedObjectContext:context];
    return c;
}

+(NSManagedObjectContext*)getContext{
    id appDelegate = [UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    return context;
}

-(NSArray<CITHeroEntity *> *)getAllHeros{
    
    NSManagedObjectContext *context = [CITHeroDBCoreData getContext];
    NSString * stringClass = NSStringFromClass([CITHeroEntity class]);
    NSEntityDescription *entity = [NSEntityDescription entityForName:stringClass inManagedObjectContext:context];
    NSFetchRequest *request = [NSFetchRequest new];
    request.entity = entity;
    
    NSSortDescriptor *sortedDescription = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    id sortedDescriptors = @[sortedDescription];
    request.sortDescriptors = sortedDescriptors;
    
    NSError *error = nil;
    NSArray<CITHeroEntity*> *array = [context executeFetchRequest:request error:&error];
    if(error){
        NSLog(@"Erro ao recuperar lista de herois no banco: %@", error.description);
    }
    return array;
}

-(CITHeroEntity*)getHeroById:(NSString*)idHero{
    
    NSManagedObjectContext *context = [CITHeroDBCoreData getContext];
    NSString * stringClass = NSStringFromClass([CITHeroEntity class]);
    NSEntityDescription *entity = [NSEntityDescription entityForName:stringClass inManagedObjectContext:context];
    NSFetchRequest *request = [NSFetchRequest new];
    request.entity = entity;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id=%@",idHero];
    request.predicate = predicate;
    NSError *error = nil;
    NSArray<CITHeroEntity*>*hero = [context executeFetchRequest:request error:&error];
    if (hero == nil) {
        NSLog(@"Error: %@, %@", error, [error userInfo]);
        return nil;
    } else if ([hero count] > 0) {
        return [hero objectAtIndex:0];
    }
    return nil;
}


-(void)save:(CITHeroEntity *)heroEntity callback:(void (^)(NSString *error))callback{
    NSManagedObjectContext *context = [CITHeroDBCoreData getContext];
    NSError *error = nil;
    [context save:&error];
    callback(error.description);
}

-(void)deleteHero:(CITHeroEntity*)hero callback:(void (^)(NSString *error))callback{
    NSManagedObjectContext *context = [CITHeroDBCoreData getContext];
    [context deleteObject:hero];
    NSError *error = nil;
    [context save:&error];
    callback(error.description);
}

@end
