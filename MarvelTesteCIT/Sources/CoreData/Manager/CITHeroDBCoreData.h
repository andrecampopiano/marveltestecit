//
//  CITHeroDBCoreData.h
//  MarvelTesteCIT
//
//  Created by André Campopiano on 28/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CITHeroEntity.h"

@interface CITHeroDBCoreData : NSObject

+(id)newInstanceForClass:(Class)classe;

-(NSArray<CITHeroEntity*>*) getAllHeros;
-(void)save:(CITHeroEntity*)heroEntity callback:(void (^)(NSString *error))callback;
-(void)deleteHero:(CITHeroEntity*)hero callback:(void (^)(NSString *error))callback;
-(CITHeroEntity*)getHeroById:(NSString*)idHero;
@end
