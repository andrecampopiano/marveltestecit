//
//  UIViewController+Utils.h
//  MarvelTesteCIT
//
//  Created by André Campopiano on 28/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Utils)

-(void) alertWithTitle:(NSString *)title message:(NSString *)msg block:(void(^)())block;
-(void) alertWithTitle:(NSString *)title message:(NSString *)msg;
@end
