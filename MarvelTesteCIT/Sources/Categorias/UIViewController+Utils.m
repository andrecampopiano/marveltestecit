//
//  UIViewController+Utils.m
//  MarvelTesteCIT
//
//  Created by André Campopiano on 28/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import "UIViewController+Utils.h"

@implementation UIViewController (Utils)

-(void) alertWithTitle:(NSString *)title message:(NSString *)msg block:(void(^)())block{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:block];
    [alertController addAction:okAction];
    
    //selector usado para executar um metodo
    [self performSelectorOnMainThread:@selector(presentViewController:) withObject:alertController waitUntilDone:NO];
}

-(void)alertWithTitle:(NSString *)title message:(NSString *)msg{
    [self  alertWithTitle:title message:msg block:nil];
}

-(void)presentViewController:(UIAlertController *) alert {
    [self presentViewController:alert animated:YES completion:nil];
}
@end
