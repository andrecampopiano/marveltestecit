//
//  NSString+Utils.h
//  MarvelTesteCIT
//
//  Created by André Campopiano on 27/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utils)

//Remove os espaços e os caracteres \n do final da string
- (NSString*)trimAndNewLine;
//Concatena as string
- (NSString*)concat:(NSString*)string;
//Subistitui as ocorrências de uma string por outra string
- (NSString*)substituir:(NSString*)string por:(NSString*)outraString;
@end
