//
//  NSString+MD5Addition.m
//  UIDeviceAddition
//
//  Created by Georg Kitz on 20.08.11.
//  Copyright 2011 Aurora Apps. All rights reserved.
//

#import "NSString+MD5Addition.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString(MD5Addition)

- (NSString *) stringFromMD5
{
    
    if(self == nil || [self length] == 0)
        return nil;
    
    const char *value = [self UTF8String];
    
    unsigned char outputBuffer[CC_MD5_DIGEST_LENGTH];
    CC_MD5(value, strlen(value), outputBuffer);
    
    NSMutableString *outputString = [[NSMutableString alloc] initWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(NSInteger count = 0; count < CC_MD5_DIGEST_LENGTH; count++){
        [outputString appendFormat:@"%02x",outputBuffer[count]];
    }
    
    return outputString;
}

-(NSData *)geraMd5ByteArray
{
    if(self == nil || [self length] == 0)
        return nil;
    
    const char *value = [self UTF8String];
    
    unsigned char outputBuffer[CC_MD5_DIGEST_LENGTH];
    CC_MD5(value, strlen(value), outputBuffer);
    
    NSData *dados = [[NSData alloc] initWithBytes:outputBuffer length:CC_MD5_DIGEST_LENGTH];
    
    return dados;
}

- (NSString *)lookLikeUniqueIdentifier {
    NSString *code = [[NSString stringWithString:self] uppercaseString];
    @try {
        code = [NSString stringWithFormat: @"%@-%@-%@-%@-%@",
            [code substringToIndex:8],
            [code substringWithRange: NSMakeRange(8,4)],
            [code substringWithRange:NSMakeRange(12,4)],
            [code substringWithRange:NSMakeRange(16,4)],
            [code substringFromIndex:20]];
            
    }
    @catch (NSException *exception) {
        code = [NSString stringWithString:self];
    }
    @finally {
        return code;
    }
}

@end
