//
//  NSString+MD5Addition.h
//  UIDeviceAddition
//
//  Created by Georg Kitz on 20.08.11.
//  Copyright 2011 Aurora Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

/*! 
 * Category utilitarian for generation of hex string from md5 hash.
 */
@interface NSString(MD5Addition)

/*!
 * Responsible for generating hex string from md5 hash.
 */
- (NSString *) stringFromMD5;


- (NSData*)geraMd5ByteArray;

- (NSString *)lookLikeUniqueIdentifier;

@end
