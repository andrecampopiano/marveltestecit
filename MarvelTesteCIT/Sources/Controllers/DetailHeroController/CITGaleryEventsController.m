//
//  CITGaleryEventsController.m
//  MarvelTesteCIT
//
//  Created by André Campopiano on 27/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import "CITGaleryEventsController.h"
#import "CITEventCell.h"
#import "CITEvents.h"
#import "CITEventService.h"
#import "CITDetailEventController.h"

@interface CITGaleryEventsController () <UICollectionViewDelegateFlowLayout>

@property(nonatomic,strong)NSMutableArray<CITEvents> *events;

@end

@implementation CITGaleryEventsController

static NSString * const cellEventIdentifier = @"cellEvent";
static NSString * const segueDetailEvent = @"segueDetailEvent";

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.collectionView registerNib:[UINib nibWithNibName:@"CITEventCell" bundle:nil] forCellWithReuseIdentifier:cellEventIdentifier];
    [self getListEvents];
    
}

-(void)getListEvents{
    [[CITEventService instance]recoveryEventWithHero:self.idHero callback:^(NSMutableArray<CITEvents> *listEvents, NSString *erro) {
        if(erro){
            
        }else {
            self.events = listEvents;
            if(self.events.count > 0){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.collectionView reloadData];
                });
            }else{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Attention" message:@"No event found for this hero" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:okAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
    }];
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:segueDetailEvent]){
        CITDetailEventController *detailController = segue.destinationViewController;
        detailController.event = (CITEvents*)sender;
    }
    
    
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.events.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CITEventCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellEventIdentifier forIndexPath:indexPath];
    CITEvents *event = self.events[indexPath.item];
    
    NSString *urlImage = [NSString stringWithFormat:@"%@.%@",event.thumbnail.path,event.thumbnail.extension];
    [cell setImgEventWithUrl:urlImage];
    return cell;
}

#pragma mark UICollectionViewDelegate
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    CITEvents *event = self.events[indexPath.item];
    [self performSegueWithIdentifier:segueDetailEvent sender:event];
}



#pragma mark UICollectionViewDelegateFlowLayout

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat width = (self.collectionView.frame.size.width - 3) / 4;
    CGFloat height = width * 1.6;
    return CGSizeMake(width, height);
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 1;
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 1;
}

@end
