//
//  CITDetailHeroController.h
//  MarvelTesteCIT
//
//  Created by André Campopiano on 26/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CITHero.h"

@interface CITDetailHeroController : UIViewController

@property(nonatomic,strong)CITHero *hero;
@end
