//
//  CITGaleryEventsController.h
//  MarvelTesteCIT
//
//  Created by André Campopiano on 27/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CITGaleryEventsController : UICollectionViewController

@property(nonatomic,strong)NSNumber *idHero;

@end
