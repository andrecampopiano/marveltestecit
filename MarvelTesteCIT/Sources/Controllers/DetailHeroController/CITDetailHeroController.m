//
//  CITDetailHeroController.m
//  MarvelTesteCIT
//
//  Created by André Campopiano on 26/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import "CITDetailHeroController.h"
#import "CITDownloadImageView.h"
#import "CITGaleryEventsController.h"
#import "CITHeroBusiness.h"
#import "UIViewController+Utils.h"

@interface CITDetailHeroController ()
@property (weak, nonatomic) IBOutlet CITDownloadImageView *imageHero;
@property (weak, nonatomic) IBOutlet CITDownloadImageView *backgroundImageHero;
@property (weak, nonatomic) IBOutlet UILabel *lblHeroName;
@property (weak, nonatomic) IBOutlet UITextView *txtDescreptionHero;
@property (weak, nonatomic) IBOutlet UIButton *btnFavorite;

@end

@implementation CITDetailHeroController

static NSString *segueGalaryEvent = @"segueGalaryEvent";


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setInfoHero];
}


-(void)setInfoHero{
    
    self.lblHeroName.text = self.hero.name;
    self.txtDescreptionHero.text = (![self.hero.descr isEqualToString:@""]) ? self.hero.descr : @"Sorry, description of this hero was not found :(";
    
    NSString *urlImage = [NSString stringWithFormat:@"%@.%@",self.hero.thumbnail.path,self.hero.thumbnail.extension];
    [self.imageHero setURL:urlImage];
    [self.backgroundImageHero setURL:urlImage];
    
    [self setBtnFavorite];
    
}


-(void)setBtnFavorite{
    if(self.hero.favorite){
        [self.btnFavorite setBackgroundImage:[UIImage imageNamed:@"icon_favorite_select"] forState:UIControlStateNormal];
    }else{
        [self.btnFavorite setBackgroundImage:[UIImage imageNamed:@"icon_favorite"] forState:UIControlStateNormal];
    }
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:segueGalaryEvent]){
        CITGaleryEventsController *galeryController = segue.destinationViewController;
        galeryController.idHero = self.hero.id;
    }
}

- (IBAction)btnFavoriteAction:(id)sender {
    self.hero.favorite = !self.hero.favorite;
    self.hero.favorite ? [self saveHero:self.hero] : [self deleteHero:[self.hero.id stringValue]];
    [self setBtnFavorite];
}

-(void)saveHero:(CITHero*)hero{
    [[CITHeroBusiness instance] saveHero:hero callback:^(NSString *erro) {
        [self alertWithTitle:@"attention" message:erro];
    }];
}

-(void)deleteHero:(NSString*)idHero{
    [[CITHeroBusiness instance]deleteHero:idHero callback:^(NSString *erro) {
        [self alertWithTitle:@"attention" message:erro];
    }];
}

@end
