//
//  CITEventCell.m
//  MarvelTesteCIT
//
//  Created by André Campopiano on 27/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import "CITEventCell.h"
#import "CITDownloadImageView.h"

@interface CITEventCell()
@property (weak, nonatomic) IBOutlet CITDownloadImageView *imgEvent;

@end

@implementation CITEventCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

-(void)setImgEventWithUrl:(NSString*)url{
    [self.imgEvent setURL:url];
}
@end
