//
//  CITEventCell.h
//  MarvelTesteCIT
//
//  Created by André Campopiano on 27/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CITEventCell : UICollectionViewCell


-(void)setImgEventWithUrl:(NSString*)url;

@end
