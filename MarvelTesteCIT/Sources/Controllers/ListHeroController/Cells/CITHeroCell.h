//
//  CITHeroCell.h
//  MarvelTesteCIT
//
//  Created by André Campopiano on 26/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CITHeroCellDelegate <NSObject>

-(void)favoriteAction:(UIButton *)button;

@end

@interface CITHeroCell : UITableViewCell

@property(nonatomic,strong) id<CITHeroCellDelegate> delegate;


-(void)setImageAndBackgroundImageWith:(NSString*)url;
-(void)setNameHero:(NSString*)name;
-(void)setDescriptionHero:(NSString*)description;
-(void)setImageBtnFavorite:(BOOL)favorite;



@end
