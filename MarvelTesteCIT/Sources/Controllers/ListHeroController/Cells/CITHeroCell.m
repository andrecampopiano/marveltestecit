//
//  CITHeroCell.m
//  MarvelTesteCIT
//
//  Created by André Campopiano on 26/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import "CITHeroCell.h"
#import "CITDownloadImageView.h"

@interface CITHeroCell()
@property (weak, nonatomic) IBOutlet CITDownloadImageView *backgroundImageHero;
@property (weak, nonatomic) IBOutlet CITDownloadImageView *imageHero;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleHero;
@property (weak, nonatomic) IBOutlet UILabel *lblDescriptionHero;
@property (weak, nonatomic) IBOutlet UIButton *btnFavorite;

@end

@implementation CITHeroCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setImageAndBackgroundImageWith:(NSString *)url{
    [self.backgroundImageHero setURL:url];
    [self.imageHero setURL:url];
}

-(void)setNameHero:(NSString *)name{
    self.lblTitleHero.text = name;
}

-(void)setDescriptionHero:(NSString *)description{
    self.lblDescriptionHero.text = (![description isEqualToString:@""]) ? description : @"Sorry, description of this hero was not found :(";
}
- (IBAction)btnFavoriteAction:(id)sender {
    [self.delegate favoriteAction:self.btnFavorite];
    
}

-(void)setImageBtnFavorite:(BOOL)favorite{
    NSString *imageBtn = favorite ? @"icon_favorite_select" : @"icon_favorite";
    [self.btnFavorite setBackgroundImage:[UIImage imageNamed:imageBtn] forState:UIControlStateNormal];
    
}



@end
