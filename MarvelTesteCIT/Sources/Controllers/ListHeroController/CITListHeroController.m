//
//  CITListHeroController.m
//  MarvelTesteCIT
//
//  Created by André Campopiano on 26/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import "CITListHeroController.h"
#import "CITHeroService.h"
#import "CITHero.h"
#import "CITHeroCell.h"
#import "CITDetailHeroController.h"
#import "UIViewController+Utils.h"
#import "CITHeroBusiness.h"
#import "NSString+Utils.h"

@interface CITListHeroController () <UITableViewDelegate,UITableViewDataSource,CITHeroCellDelegate,UISearchBarDelegate>{
    BOOL nextList;
    NSMutableArray<CITHero> *listHeroOriginal;
    
}
@property(nonatomic,strong)NSMutableArray<CITHero> *listHero;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@property (nonatomic, assign) NSInteger offSet;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;


@end

@implementation CITListHeroController

static NSString *cellHeroIdenfield = @"cellHeroIdenfield";
static NSString *segueDetailHero = @"segueDetailHero";


- (void)viewDidLoad {
    [super viewDidLoad];
    listHeroOriginal = [NSMutableArray<CITHero> new];
    nextList = YES;
    [self setupLayout];
    self.searchBar.delegate = self;
    self.automaticallyAdjustsScrollViewInsets = false;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerNib:[UINib nibWithNibName:@"CITHeroCell" bundle:nil] forCellReuseIdentifier:cellHeroIdenfield];
    [self getListHeroWithOffSet:self.listHero.count];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self reloadData];
}

#pragma mark - customLayout
-(void)setupLayout{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.tableView.bounds;
    gradient.colors = @[(id)[UIColor colorWithRed:0.0 green:0.29 blue:0.58 alpha:1.0].CGColor, (id)[UIColor colorWithRed:0.0 green:0.62 blue:1.0 alpha:1.0].CGColor];
    [self.view.layer insertSublayer:gradient atIndex:0];
    UIView * backgroundView = [[UIView new]initWithFrame:self.tableView.bounds];
    [backgroundView.layer insertSublayer:gradient atIndex:0];
    self.tableView.backgroundView = backgroundView;
}

-(void)getListHeroWithOffSet:(NSInteger)offset{
    [self.activity setHidden:NO];
    [self.activity startAnimating];
    [[UIApplication sharedApplication]beginIgnoringInteractionEvents];
    [[CITHeroService instance]recoveryHeroesDbAndApiWithOffSet:offset callback:^(NSMutableArray<CITHero> *listHero, NSString *erro) {
        if(erro){
            [self alertWithTitle:@"attention" message:erro block:nil];
        }
        self.offSet = 30;
        listHeroOriginal = listHero;
        self.listHero = listHero;
        [self reloadData];
    }];
}

-(void)reloadData{
    NSSortDescriptor *sortFavorite = [NSSortDescriptor sortDescriptorWithKey:@"favorite" ascending:NO];
    NSSortDescriptor *sortName = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    self.listHero = (NSMutableArray<CITHero>*)[self.listHero sortedArrayUsingDescriptors:@[sortFavorite,sortName]];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
        [self.activity stopAnimating];
        [self.activity setHidden:YES];
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    });
}


#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CITHero *hero = self.listHero[indexPath.row];
    [self performSegueWithIdentifier:segueDetailHero sender:hero];
    
}

#pragma mark - UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.listHero.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CITHeroCell *cell = [tableView dequeueReusableCellWithIdentifier:cellHeroIdenfield forIndexPath:indexPath];
    cell.delegate = self;
    CITHero *hero = self.listHero[indexPath.row];
    [cell setNameHero:hero.name];
    NSString *urlImage = [NSString stringWithFormat:@"%@.%@",hero.thumbnail.path,hero.thumbnail.extension];
    [cell setImageAndBackgroundImageWith:urlImage];
    [cell setDescriptionHero:hero.descr];
    [cell setImageBtnFavorite:hero.favorite];
    return cell;
}


#pragma mark - UIScrollView
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if(self.tableView.contentOffset.y >= (self.tableView.contentSize.height - self.tableView.bounds.size.height) && nextList) {
        nextList = NO;
            [self.activity setHidden:NO];
            [self.activity startAnimating];
            [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
            [[CITHeroService instance]recoveryHerosWithOffSet:self.offSet callback:^(NSMutableArray<CITHero> *listHero, NSString *erro) {
                if(erro){
                    [self alertWithTitle:@"attention" message:@"Erro ao atualizar"];
                }else{
                    NSMutableArray<CITHero>* newListHero = [NSMutableArray<CITHero> new];
                    [newListHero addObjectsFromArray:self.listHero];
                    NSMutableArray<NSIndexPath *> *arrayIndex = [NSMutableArray<NSIndexPath *> new];
                    for(CITHero *hero in listHero){
                        [newListHero addObject:hero];
                        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:newListHero.count -1  inSection:0];
                        [arrayIndex addObject:indexPath];
                    }
                    self.offSet = self.offSet + 30;
                    self.listHero = newListHero;
                    listHeroOriginal = newListHero;
                    nextList = YES;
                    [self reloadData];
                }
            }];
        
    }
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:segueDetailHero]){
        CITDetailHeroController *detailController = segue.destinationViewController;
        detailController.hero = (CITHero*)sender;
    }
}

#pragma mark - CITHeroCellDelegate
-(void)favoriteAction:(UIButton *)button{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:(UITableViewCell*)button.superview.superview];
    CITHero *hero = self.listHero[indexPath.row];
    hero.favorite = !hero.favorite;
    NSString *imageBtn = hero.favorite ? @"icon_favorite_select" : @"icon_favorite";
    [button setBackgroundImage:[UIImage imageNamed:imageBtn] forState:UIControlStateNormal];
    hero.favorite ? [self saveHero:hero] : [self deleteHero:[hero.id stringValue]];
    [self reloadData];
}

#pragma mark - Adiciona e remove favoritos
-(void)saveHero:(CITHero*)hero{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id=%@",hero.id];
    NSMutableArray<CITHero> *list = (NSMutableArray<CITHero>*)[listHeroOriginal filteredArrayUsingPredicate:predicate];
    if(list.count == 0){
        [listHeroOriginal addObject:hero];
    }
    
    
    
    [[CITHeroBusiness instance]saveHero:hero callback:^(NSString *erro) {
        [self alertWithTitle:@"attention" message:erro block:nil];
    }];
}

-(void)deleteHero:(NSString*)idHero{
    [[CITHeroBusiness instance]deleteHero:idHero callback:^(NSString *erro) {
        [self alertWithTitle:@"attention" message:erro block:nil];
    }];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    if(![searchBar.text isEqualToString:[@"" trimAndNewLine]]){
        nextList = NO;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name contains[cd]  %@",searchBar.text];
        NSMutableArray<CITHero> *listHeroFilter = (NSMutableArray<CITHero>*)[self.listHero filteredArrayUsingPredicate:predicate];
        if(listHeroFilter.count < 1){
            [[CITHeroService instance]recoveryHerosWithName:searchBar.text callback:^(NSMutableArray<CITHero> *listHero, NSString *erro) {
                if(listHero.count > 0){
                    self.listHero = listHero;
                }else{
                    [self alertWithTitle:@"attention" message:@"No Hero Found"];
                    self.listHero = listHeroOriginal;
                }
                [self reloadData];
            }];
        }else{
            self.listHero = listHeroFilter;
            [self reloadData];
        }
    }else{
        nextList = YES;
        self.listHero = listHeroOriginal;
        [self reloadData];
    }
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    nextList = YES;
    self.listHero = listHeroOriginal;
    [self reloadData];

  }

@end
