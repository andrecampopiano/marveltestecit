//
//  CITDetailEventController.m
//  MarvelTesteCIT
//
//  Created by André Campopiano on 27/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import "CITDetailEventController.h"
#import "CITDownloadImageView.h"

@interface CITDetailEventController ()
@property (weak, nonatomic) IBOutlet UILabel *lblTitleEvent;
@property (weak, nonatomic) IBOutlet CITDownloadImageView *imageEvent;
@property (weak, nonatomic) IBOutlet UITextView *txtDescrEvent;

@end

@implementation CITDetailEventController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.lblTitleEvent.text = self.event.title;
    NSString *urlImage = [NSString stringWithFormat:@"%@.%@",self.event.thumbnail.path,self.event.thumbnail.extension];
    [self.imageEvent setURL:urlImage];
    self.txtDescrEvent.text = self.event._description;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
