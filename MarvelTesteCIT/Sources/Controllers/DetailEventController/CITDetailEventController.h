//
//  CITDetailEventController.h
//  MarvelTesteCIT
//
//  Created by André Campopiano on 27/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CITEvents.h"

@interface CITDetailEventController : UIViewController

@property(nonatomic,strong)CITEvents *event;

@end
