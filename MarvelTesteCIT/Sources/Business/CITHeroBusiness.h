//
//  CITHeroBusiness.h
//  MarvelTesteCIT
//
//  Created by André Campopiano on 28/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CITHero.h"

@interface CITHeroBusiness : NSObject

+ (instancetype) instance;

-(void)saveHero:(CITHero*)hero callback:(void (^)(NSString *erro))callback;
-(void)deleteHero:(NSString*)idHero callback:(void (^)(NSString *erro))callback;

@end
