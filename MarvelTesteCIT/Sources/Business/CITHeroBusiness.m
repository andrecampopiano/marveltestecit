//
//  CITHeroBusiness.m
//  MarvelTesteCIT
//
//  Created by André Campopiano on 28/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import "CITHeroBusiness.h"
#import "CITHeroService.h"

@implementation CITHeroBusiness

static CITHeroBusiness *instance = nil;

+(instancetype) instance {
    @synchronized ([CITHeroBusiness class]) {
        if (instance == nil){
            instance = [[super alloc]init];
        }
        return instance;
    };
    return nil;
}

-(void)saveHero:(CITHero *)hero callback:(void (^)(NSString *))callback{
    [[CITHeroService instance]saveHero:hero callback:^(NSString *erro) {
        if(erro){
            callback(erro);
        }else {
            callback(@"Hero added to favorites successfully");
        }
    }];
}


-(void)deleteHero:(NSString *)idHero callback:(void (^)(NSString *erro))callback{
    [[CITHeroService instance]deleteHero:idHero callback:^(NSString *erro) {
        if(erro){
            callback(erro);
        }else{
            callback(@"Hero successfully removed from favorites");
        }
    }];
}
@end
