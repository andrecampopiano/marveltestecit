//
//  CITEvents.m
//  MarvelTesteCIT
//
//  Created by André Campopiano on 26/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import "CITEvents.h"

@implementation CITEvents

+(JSONKeyMapper *)keyMapper{
    
    return [[JSONKeyMapper alloc]initWithModelToJSONDictionary:@{@"id":@"id",@"title":@"title",@"_description": @"description",@"modified":@"modified", @"resourceURI":@"resourceURI", @"thumbnail":@"thumbnail", @"comics": @"comics",
                                                                 @"series":@"series", @"stories":@"stories",@"events":@"events",@"urls":@"urls",@"start":@"start",@"end":@"end",@"creators":@"creators",@"characters":@"characters",@"next":@"next",@"previous":@"previous"}];
}




@end
