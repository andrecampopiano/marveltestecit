//
//  CITHero.h
//  MarvelTesteCIT
//
//  Created by André Campopiano on 26/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "CITThumbnail.h"
@interface CITHero : JSONModel
@property(nonatomic,strong) NSNumber *id;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *descr;
@property(nonatomic,strong) NSString *modified;
@property(nonatomic,strong) NSString *resourceURI;
@property(nonatomic,strong) CITThumbnail *thumbnail;
@property(nonatomic,strong) NSObject<Ignore> *comics;
@property(nonatomic,strong) NSObject<Ignore> *series;
@property(nonatomic,strong) NSObject<Ignore> *stories;
@property(nonatomic,strong) NSObject <Ignore> *events;
@property(nonatomic,strong) NSArray<Ignore> *urls;
@property(nonatomic,assign) BOOL favorite;
@end

@protocol CITHero

@end
