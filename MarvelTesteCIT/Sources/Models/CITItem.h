//
//  CITItem.h
//  MarvelTesteCIT
//
//  Created by André Campopiano on 26/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface CITItem : JSONModel

@property(nonatomic,strong)NSString *resourceURI;
@property(nonatomic,strong)NSString *name;
@property(nonatomic,strong)NSString<Optional> *type;
@end

@protocol CITItem
@end
