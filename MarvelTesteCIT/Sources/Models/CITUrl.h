//
//  CITUrl.h
//  MarvelTesteCIT
//
//  Created by André Campopiano on 26/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface CITUrl : JSONModel

@property(nonatomic,strong)NSString *type;
@property(nonatomic,strong)NSString *url;
@end

@protocol CITUrl
@end
