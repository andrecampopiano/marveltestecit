//
//  CITData.h
//  MarvelTesteCIT
//
//  Created by André Campopiano on 26/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "CITHero.h"

@interface CITData : JSONModel
@property(nonatomic,strong)NSNumber *offset;
@property(nonatomic,strong)NSNumber *limit;
@property(nonatomic,strong)NSNumber *total;
@property(nonatomic,strong)NSNumber *count;
@property(nonatomic,strong)NSMutableArray *results;
@end

@protocol CITData

@end
