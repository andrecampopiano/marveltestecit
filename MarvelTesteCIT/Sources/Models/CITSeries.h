//
//  CITSeries.h
//  MarvelTesteCIT
//
//  Created by André Campopiano on 26/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "CITItem.h"

@interface CITSeries : JSONModel
@property(nonatomic,strong) NSNumber *available;
@property(nonatomic,strong) NSString *collectionURI;
@property(nonatomic,strong) NSNumber *returned;
@property(nonatomic,strong) NSMutableArray<CITItem> *items;
@end

@protocol CITSeries
@end
