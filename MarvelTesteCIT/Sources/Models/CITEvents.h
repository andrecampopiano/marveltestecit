//
//  CITEvents.h
//  MarvelTesteCIT
//
//  Created by André Campopiano on 26/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import <JSONModel/JSONModel.h>

#import "CITThumbnail.h"


@interface CITEvents : JSONModel

@property(nonatomic,strong) NSNumber *id;
@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *_description;
@property(nonatomic,strong) NSString *resourceURI;
@property(nonatomic,strong) NSArray<Ignore> *urls;
@property(nonatomic,strong) NSString *modified;
@property(nonatomic,strong) CITThumbnail *thumbnail;
@property(nonatomic,strong) NSString *start;
@property(nonatomic,strong) NSString *end;
@property(nonatomic,strong) NSArray<Ignore> *creators;
@property(nonatomic,strong) NSObject<Ignore> *stories;
@property(nonatomic,strong) NSArray<Ignore> *characters;
@property(nonatomic,strong) NSObject<Ignore> *next;
@property(nonatomic,strong) NSObject<Ignore> *previous;
@property(nonatomic,strong) NSObject<Ignore> *comics;
@property(nonatomic,strong) NSObject<Ignore> *series;

@end
@protocol CITEvents
@end

