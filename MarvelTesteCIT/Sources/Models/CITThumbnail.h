//
//  CITThumbnail.h
//  MarvelTesteCIT
//
//  Created by André Campopiano on 26/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface CITThumbnail : JSONModel

@property(nonatomic,strong) NSString *path;
@property(nonatomic,strong) NSString *extension;

@end

@protocol CITThumbnail

@end
