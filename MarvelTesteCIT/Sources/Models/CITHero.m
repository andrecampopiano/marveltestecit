//
//  CITHero.m
//  MarvelTesteCIT
//
//  Created by André Campopiano on 26/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import "CITHero.h"

@implementation CITHero

+(JSONKeyMapper *)keyMapper{
    
    return [[JSONKeyMapper alloc]initWithModelToJSONDictionary:@{@"id":@"id",@"name":@"name",@"descr": @"description",@"modified":@"modified", @"resourceURI":@"resourceURI", @"thumbnail":@"thumbnail", @"comics": @"comics",
                                                                 @"series":@"series", @"stories":@"stories",@"events":@"events",@"urls":@"urls"}];
    }



+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    if ([propertyName isEqualToString: @"favorite"]) return YES;
    return NO;
}

@end
