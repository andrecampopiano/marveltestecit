//
//  CITConector.h
//  MarvelTesteCIT
//
//  Created by André Campopiano on 26/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIImage.h>

typedef NS_ENUM(NSInteger, CITConnectionType) {
    CITConnectionTypePost,
    CITConnectionTypePut,
    CITConnectionTypeGet,
    CITConnectionTypeDelete
};

typedef void(^CITReturnConnectorBlock)(NSURLResponse* response, id data, NSError* connectionError);


@interface CITConector : NSObject<NSURLSessionDelegate>

+(instancetype) instance;
+(id) alloc __attribute__((unavailable("alloc not available")));
-(void) call:(NSString*)urlServico method:(CITConnectionType)type completion:(CITReturnConnectorBlock)completion;
-(void) call:(NSString*)urlServico method:(CITConnectionType)type body:(id) body completion: (CITReturnConnectorBlock)completion;
@end
