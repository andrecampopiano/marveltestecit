//
//  CITConector.m
//  MarvelTesteCIT
//
//  Created by André Campopiano on 26/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import "CITConector.h"
#import "CITConfig.h"
#import "NSString+MD5Addition.h"

@interface CITConector(){
    NSURLSession *_urlSession;
    NSString *api_path;
    NSString *images_path;
    CITConfig *config;
    
}
@end

@implementation CITConector

static CITConector *instance = nil;

+(instancetype)instance{
    @synchronized ([CITConector class]) {
        if(instance == nil){
            instance = [[super alloc]init];
        }
        return instance;
    }
    return nil;
}

-(id) init {
    if(self = [super init]){
        _urlSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:self delegateQueue:nil];
    }
    
    config = [CITConfig instance];
    api_path = [NSString stringWithFormat:@"%@://%@%@", [config protocol], [config host] , [config apiBasePath]];
    
    return self;
    
}

-(void)call:(NSString *)urlServico method:(CITConnectionType)type completion:(CITReturnConnectorBlock)completion{
    [self call:urlServico method:type body:nil completion:^(NSURLResponse *response, id data, NSError *connectionError) {
        completion(response, data, connectionError);
    }];
}

-(void)call:(NSString *)urlServico method:(CITConnectionType)type body:(id)body completion:(CITReturnConnectorBlock)completion{
    
    NSString *ts = [self getTimeStempString];
    NSString *hash = [self configHashWithTimeStampString:ts];
    
    NSString *urlString = [api_path stringByAppendingString:urlServico];
    urlString = [urlString stringByAppendingString:[NSString stringWithFormat:@"limit=%@&ts=%@&apikey=%@&hash=%@",[config limit],ts,[config publicKey],hash]];
    
    
    NSURL *url =[NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSURLSessionDataTask *dataTask = [_urlSession dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        completion(response,data,error);
    }];
    
    [dataTask resume];
    
}

-(NSString *)connectionTypeToString:(CITConnectionType)method{
    switch (method) {
        case CITConnectionTypePost:
            return @"POST";
        case CITConnectionTypeGet:
            return @"GET";
        case CITConnectionTypePut:
            return @"PUT";
        case CITConnectionTypeDelete:
            return @"DELETE";
    }
}

-(NSString*)getTimeStempString{
    NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
    NSString *ts = [NSString stringWithFormat:@"%f",timeStamp];
    return ts;
}



-(NSString*)configHashWithTimeStampString:(NSString*)ts{
    NSString *hash = [[NSString stringWithFormat:@"%@%@%@",ts, [config privateKey],[config publicKey]] stringFromMD5];
    return hash;
}

@end
