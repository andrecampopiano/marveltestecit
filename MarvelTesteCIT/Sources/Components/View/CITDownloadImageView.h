//
//  CITDownloadImageView.h
//  MarvelTesteCIT
//
//  Created by André Campopiano on 26/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CITDownloadImageView : UIImageView

-(void)setURL:(NSString*)url;
@end
