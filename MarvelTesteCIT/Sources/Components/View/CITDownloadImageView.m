//
//  CITDownloadImageView.m
//  MarvelTesteCIT
//
//  Created by André Campopiano on 26/05/17.
//  Copyright © 2017 André Campopiano. All rights reserved.
//

#import "CITDownloadImageView.h"
#import "NSString+Utils.h"
@interface CITDownloadImageView(){
    UIActivityIndicatorView *progress;
    NSOperationQueue *mainQueue;
    NSOperationQueue *queue;
    
}
@end
@implementation CITDownloadImageView

-(instancetype)initWithCoder:(NSCoder *)aDecoder   {
    queue = [NSOperationQueue new];
    mainQueue = [NSOperationQueue mainQueue];
    return [super initWithCoder:aDecoder];
}

-(void)createProgress{
    progress = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [progress setHidesWhenStopped:YES];
    [self addSubview:progress];
}

-(void)layoutSubviews{
    progress.center = [self convertPoint:self.center toView:self];
}

-(void)setURL:(NSString *)url{
    [self createProgress];
    [self setURL:url cache:YES];
}

-(void)setURL:(NSString *)url cache:(BOOL)cache{
    self.image= nil;
    [queue cancelAllOperations];
    [progress startAnimating];
    
    
    [queue addOperationWithBlock:^{
        [self downloadImage:url cache:cache];
    }];
    
}

-(void)downloadImage:(NSString*)url cache:(BOOL)cache{
    NSData *data = nil;
    
    if(!cache){
        NSURL *urlImage = [NSURL URLWithString:url];
        data = [NSData dataWithContentsOfURL:urlImage];
    }else{
        NSString *path = [url substituir:@"/" por:@"_"];
        path = [path substituir:@"\\" por:@"_"];
        path = [path substituir:@"." por:@"_"];
        path = [[NSHomeDirectory() concat:@"/Documents/"] concat:path];
        
        BOOL existe = [[NSFileManager defaultManager]fileExistsAtPath:path];
        if(existe){
            data = [NSData dataWithContentsOfFile:path];
            NSLog(@"cache");
        }else{
            NSLog(@"url");
            NSURL *urlImage = [NSURL URLWithString:url];
            data = [NSData dataWithContentsOfURL:urlImage];
            UIImage *image = [UIImage imageWithData:data];
            if(image !=nil){
                [data writeToFile:path atomically:YES];
            }
        }
    }
    [mainQueue addOperationWithBlock:^{
        [self showImage:data];
    }];
}

-(void)showImage:(NSData *)data{
    if(data.length >0){
        self.image = [UIImage imageWithData:data];
    }
    [progress stopAnimating];
}
@end
